package character

import tl "github.com/JoelOtter/termloop"

type Player struct {
	*tl.Entity
	Name string
}

func CreatePlayer() Player {
	player := Player{
		Entity: tl.NewEntity(1, 1, 1, 1),
		Name:   "Player",
	}
	player.Entity.SetCell(0, 0, &tl.Cell{Fg: tl.ColorRed, Ch: '☻'})
	return player
}

func (p *Player) Tick(event tl.Event) {
	if event.Type == tl.EventKey {
		x, y := p.Entity.Position()
		switch event.Key {
		case tl.KeyArrowRight:
			p.Entity.SetPosition(x+1, y)
		case tl.KeyArrowLeft:
			p.Entity.SetPosition(x-1, y)
		case tl.KeyArrowUp:
			p.Entity.SetPosition(x, y-1)
		case tl.KeyArrowDown:
			p.Entity.SetPosition(x, y+1)
		}
	}
}
