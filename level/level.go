package level

import tl "github.com/JoelOtter/termloop"

type GohackLevel struct {
	tl.Level
	Name string
}

func GenerateTestLevel() (GohackLevel, error) {
	lvl := GohackLevel{
		Level: tl.NewBaseLevel(tl.Cell{
			Bg: tl.ColorWhite,
			Fg: tl.ColorBlack,
			Ch: 'T',
		}),
		Name: "A Test Level",
	}
	x := 5
	y := 2
	numOfRectangles := 24
	rows := 4
	maxRectsOnRow := numOfRectangles / rows
	rectangleWidth := 20
	rectangleHeight := 10
	rectangleColors := [5]tl.Attr{tl.ColorBlack, tl.ColorBlue, tl.ColorRed, tl.ColorCyan, tl.ColorYellow}
	colorPt := 0
	numOfRectsOnRow := 0
	for i := 0; i < numOfRectangles; i++ {
		lvl.Level.AddEntity(tl.NewRectangle(x, y, rectangleWidth, rectangleHeight, rectangleColors[colorPt]))
		x = x + rectangleWidth + 5
		numOfRectsOnRow++
		colorPt++
		if numOfRectsOnRow >= maxRectsOnRow {
			y = y + rectangleHeight + 2
			x = 5
			numOfRectsOnRow = 0
		}
		if colorPt >= len(rectangleColors) {
			colorPt = 0
		}
	}
	return lvl, nil
}
