package main

import (
	"gohack/character"
	"gohack/level"

	tl "github.com/JoelOtter/termloop"
)

func main() {
	game := tl.NewGame()
	lvl, _ := level.GenerateTestLevel()
	player := character.CreatePlayer()
	lvl.AddEntity(&player)
	game.Screen().SetFps(30)
	game.Screen().SetLevel(lvl.Level)
	game.Start()
}
