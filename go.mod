module gohack

go 1.21.6

require github.com/JoelOtter/termloop v0.0.0-20210806173944-5f7c38744afb

require (
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/nsf/termbox-go v1.1.1 // indirect
	github.com/rivo/uniseg v0.4.6 // indirect
)
